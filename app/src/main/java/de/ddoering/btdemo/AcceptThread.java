package de.ddoering.btdemo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.os.Handler;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by ddoering on 08.07.2015.
 */
public class AcceptThread extends Thread {
    private final BluetoothServerSocket bluetoothServerSocket;
    private Handler handler = new Handler();

    public AcceptThread(BluetoothAdapter bluetoothAdapter) {
        BluetoothServerSocket tmpSocket = null;

        try {
            tmpSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord("btDemoApp", java.util.UUID.fromString("4120eef8-2582-11e5-b345-feff819cdc9f"));
        } catch (Exception ex) {
            Log.d("BTDemo", "Fehler beim erstellen des Sockets");
        }
        bluetoothServerSocket = tmpSocket;
    }

    @Override
    public void run() {
        BluetoothSocket socket = null;
        final BluetoothSocket testSocket;

        // Keep listening until exception occurs or a socket is returned
        while (true) {
            try {
                socket = bluetoothServerSocket.accept();
            } catch (Exception e) {
                Log.d("BTDemo", "Fehler beim öffnen des Sockets");
            }
            // If a connection was accepted
            if (socket != null) {
                testSocket = socket;
                // Do work to manage the connection (in a separate thread)
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        ConnectedThread connectedThread = new ConnectedThread(testSocket);
                        connectedThread.start();
                    }
                });
                try {
                    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                    dataOutputStream.write("Hallo Welt!".getBytes());
                } catch (IOException e) {
                    Log.d("BTDemo", "Fehler beim erstellen des OutputStreams");
                }
                try {
                    bluetoothServerSocket.close();
                }catch (Exception ex) {
                    Log.d("BTDemo", "Fehler beim schließen des Sockets");
                }

                break;
            }
        }
    }

    /** Will cancel the listening socket, and cause the thread to finish */
    public void cancel() {
        try {
            bluetoothServerSocket.close();
        } catch (Exception e) {
            Log.d("BTDemo", "Fehler beim schließen des Sockets");
        }
    }
}
