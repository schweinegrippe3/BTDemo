package de.ddoering.btdemo.screens;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import de.ddoering.btdemo.ConnectThread;
import de.ddoering.btdemo.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnBtDeviceSelectionListener} interface
 * to handle interaction events.
 */
public class ScanDevicesFragment extends Fragment {

    private BluetoothAdapter bluetoothAdapter;
    private OnBtDeviceSelectionListener mListener;
    private ListView lvPairedDevices;
    private ListView lvNewDevices;
    ArrayAdapter<BluetoothDevice> newDeviceAdapter;

    private final ArrayList<BluetoothDevice> alNewDevices = new ArrayList<BluetoothDevice>();

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                alNewDevices.add(device);
                Log.d("BTDemo", device.getName() + " - " + device.getAddress());
                newDeviceAdapter.notifyDataSetChanged();
            }
        }
    };

    public ScanDevicesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_scan_devices, container, false);
        init(v);

        //Bereits gepaarte Geraete auflisten
        final ArrayList<String> alPairedDevices = new ArrayList<String>();
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if(pairedDevices.size() > 0)
            for (BluetoothDevice device : pairedDevices)
                alPairedDevices.add(device.getName() + "\n" + device.getAddress());
        final ArrayAdapter<String> pairedDeviceAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.myteytview, alPairedDevices);
        lvPairedDevices.setAdapter(pairedDeviceAdapter);

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        getActivity().getApplicationContext().registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
        newDeviceAdapter = new ArrayAdapter<BluetoothDevice>(getActivity().getApplicationContext(), R.layout.myteytview, alNewDevices);
        bluetoothAdapter.startDiscovery();
        lvNewDevices.setAdapter(newDeviceAdapter);
        lvNewDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity().getApplicationContext(), alNewDevices.get(position).toString() + " wird gepaart", Toast.LENGTH_SHORT).show();
                ConnectThread t = new ConnectThread(alNewDevices.get(position));
                t.start();
                mListener.onDeviceSelection(alNewDevices.get(position));
            }
        });


        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().getApplicationContext().unregisterReceiver(mReceiver);
        bluetoothAdapter.cancelDiscovery();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnBtDeviceSelectionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnBtDeviceSelectionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnBtDeviceSelectionListener {
        // TODO: Update argument type and name
        public void onDeviceSelection(BluetoothDevice bluetoothDevice);
    }

    private void init(View v) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        lvPairedDevices = (ListView) v.findViewById(R.id.lv_paired_devices);
        lvNewDevices = (ListView) v.findViewById(R.id.lv_found_devices);
    }

}
