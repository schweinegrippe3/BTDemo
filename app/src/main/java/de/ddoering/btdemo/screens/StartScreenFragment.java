package de.ddoering.btdemo.screens;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

import de.ddoering.btdemo.AcceptThread;
import de.ddoering.btdemo.ConnectThread;
import de.ddoering.btdemo.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link startScreenInteractionListener} interface
 * to handle interaction events.
 */
public class StartScreenFragment extends Fragment {

    private startScreenInteractionListener startScreenInteractionListener;
    public static final String ACTION_SEARCH_DEVICES = "search";
    private static int REQUEST_ENABLE_BT = 1337;

    private BluetoothAdapter btAdapter;
    private Button btnScanDevices;
    private Button btnReciveText;
    private Button btnSendText;
    private ArrayAdapter pairedDevices;
    private AcceptThread recieverThread;
    android.os.Handler handler = new android.os.Handler() {
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(getActivity().getApplicationContext(), msg.toString(), Toast.LENGTH_LONG);
        }
    };

    public StartScreenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_start_screen, container, false);
        init(v);

        btnScanDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction tn = getActivity().getFragmentManager().beginTransaction();
                tn.replace(R.id.fragment_container, new ScanDevicesFragment());
                tn.addToBackStack(null);
                tn.commit();
            }
        });

        btnReciveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieverThread = new AcceptThread(btAdapter);
                recieverThread.start();
            }

        });


        // Inflate the layout for this fragment
        return v;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*
        try {
            startScreenInteractionListener = (startScreenInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement startScreenInteractionListener");
        }
        */
    }

    @Override
    public void onDetach() {
        super.onDetach();
        startScreenInteractionListener = null;
    }

    private void init(View v)
    {
        //Hole den Adapter und bringe ihn wenn n�tig in den richtigen Zustand
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter == null)
        {
            Log.d("BTDemo", "Gerät unterstützt kein Bluetooth");
            Toast.makeText(getActivity().getApplicationContext(), "Dieses Gerät unterstützt kein Bluetooth", Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
        if(!btAdapter.isEnabled())
        {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        btnScanDevices = (Button) v.findViewById(R.id.btn_scan_devices);
        btnReciveText = (Button) v.findViewById(R.id.btn_recieve_text);
        btnSendText = (Button) v.findViewById(R.id.btn_send_text);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface startScreenInteractionListener {
        // TODO: Update argument type and name
        public void onPageChangeAction(String selectedAction);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Bluetooth An/Aus Ergbenis
        if(requestCode == REQUEST_ENABLE_BT)
        {
            if(resultCode != Activity.RESULT_OK)
            {
                //Der User hat Bluetooth nicht aktiviert...
                Toast.makeText(getActivity().getApplicationContext(), "Bluetooth muss aktiviert werden", Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
        }
    }
}
